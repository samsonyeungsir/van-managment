import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GnewuserPage } from './gnewuser.page';

const routes: Routes = [
  {
    path: '',
    component: GnewuserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GnewuserPageRoutingModule {}
