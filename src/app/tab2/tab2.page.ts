import {
  AfterContentChecked,
  Pipe,
  PipeTransform,
  Component,
  ViewChild,
  ViewEncapsulation,
  ElementRef,
} from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { RestfulService } from '../api/restful.service';
import { Storage } from '@capacitor/storage';
import { VehisericeService } from '../api/vehiserice.service';
import { Router, NavigationExtras } from '@angular/router';
import { LoadingController } from '@ionic/angular';

declare var google;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class Tab2Page {
  map: any;
  allroutearr;
  searchitem = [];
  lessvehi: string;
  morevehi: string;
  route;
  routeURL =
    'https://bmmzpyd2g8.execute-api.ap-east-1.amazonaws.com/Prod/hello/';
  routedata = [];
  stationname: any;
  lessvehiarr: any;
  getpeoplecount;
  getinform;
  routearr: any;
  peopletime;
  routedisplay;
  storedata;
  routetri: any[];
  data: any;

  constructor(
    public http: HttpClient,
    public loadingController: LoadingController,
    private router: Router
  ) {}
  ngOnInit() {
    RestfulService.setHttp(this.http);
    setTimeout(() => this.getinfor(), 100);
  }
  ionViewWillEnter() {
    setTimeout(() => this.getpeople(), 5000);
  }

  ionViewDidLeave() {}

  getroute(_item) {
    clearTimeout(this.getinform);
    clearTimeout(this.peopletime);

    Storage.set({ key: 'nextpage', value: JSON.stringify(_item) });
    setTimeout(
      () => this.router.navigateByUrl('/routedetail', { replaceUrl: true }),
      1000
    );
  }

  getpeople() {
    if (!VehisericeService.rawvehi) {
      return;
    }
    var getpeople = VehisericeService.rawvehi;

    this.routetri.forEach((vid) => {
      vid.peoplebelow = [];
      vid.peopleover = [];
    });
    getpeople.forEach((people) => {
      this.routetri.forEach((vid) => {
        if (people.route == vid.id) {
          //if (people.people != null) {
          if (people.people == null) {
          } else if (people.people <= 8) {
            vid.peoplebelow.push(people.people);

            vid.countbelow = vid.peoplebelow.length;

            Storage.set({ key: vid.id + 'countbelow', value: vid.countbelow });
          } else if (people.people > 8) {
            vid.peopleover.push(people.people);
            vid.countabove = vid.peopleover.length;
            Storage.set({ key: vid.id + 'countabove', value: vid.countabove });
          }
          //}
        }
      });
      //VehisericeService.getroutezone()
      // this.peopletime = setTimeout(() => this.getpeople(),30000);
    });
    //this.storedata= setTimeout(() =>  Storage.set({ key: "route", value:JSON.stringify(this.routetri)}), 4000)
    this.storedata = setTimeout(() => (this.route = this.routetri), 10000);
  }

  getinfor() {
    this.routetri = [];

    if (!VehisericeService.routearr) {
      return;
    }

    this.routearr = VehisericeService.routearr;
console.log('VehisericeService.routearr',VehisericeService.routearr);
    for (const [key, value] of Object.entries(this.routearr)) {
      this.data = value;
      this.data.routes.forEach((info) => {
        Storage.get({ key: info.name + 'countbelow' }).then((below) => {
          if (below.value == null) {
            var cbelow = '0';
          } else {
            var cbelow = below.value;
          }
          Storage.get({ key: info.name + 'countabove' }).then((above) => {
            if (above.value == null) {
              var cabove = '0';
            } else {
              var cabove = above.value;
            }
            var input = {
              id: info.name,
              rname: info.long_name.split(/[a-zA-Z0-9]{1,}/gm)[0],
              peopleover: [],
              peoplebelow: [],
              countabove: cabove,
              countbelow: cbelow,
             readID:key
            };
            this.routetri.push(input);
            this.routetri.sort((a, b) => {
              return a.id - b.id;
            });
      
          });
        });
      });
    }

    setTimeout(() => (this.route = this.routetri), 1000);
  }
}
