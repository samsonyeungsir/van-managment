import { Component, OnInit } from '@angular/core';
import { CognitoService } from '../api/cognito.service';
import { Storage } from '@capacitor/storage';
import { LoadingController } from '@ionic/angular';
import { VehisericeService } from '../api/vehiserice.service';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { RestfulService } from '../api/restful.service';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {
  public URLink = 'http://dev.taxieco.org/';
public sessionURL='http://apps.taxieco.org/StandardApiAction_login.action?account=apitaxi&password=t@xi7899';
  public username;
  public data;
  public JSON_RESULT;
  public SERVER_URL = 'userlogin.php';
  public company_URL = 'getcompanyname.php';
  public getVehiURL = 'callvehi.php';
  JSON_USER: any;
  public lang;
  public hashPW;
  routearr = [{ route: '60K', area: 'NT' }];
  vehidata;

  vehiarr = [];
  taxicolor = [];
  companydata;
  phonewthcode: string;
  company: any;
  comdata: any;
  loginkey: any;
  phone: string;
  password: any;
  today: string;
  yesterday: any;
  loading: void;
  arrrote: any;
  arr;
  session ;
  temp;

  constructor(
    public CognitoSerive: CognitoService,
    public loadingController: LoadingController,
    private router: Router,
    public http: HttpClient,
    public toasterCtrl: ToastController
  ) {}

  ngOnInit() {
    RestfulService.setHttp(this.http);
  }
  ionViewWillEnter() {
    //this.getdate()
    this.checklogin();
   
    this.getsession() 
   // setTimeout(() => this.test(), 1500);
  }

  checklogin() {
    Storage.get({ key: 'password' }).then((val) => {
      this.password = JSON.parse(val.value);
      console.log('this.password',this.password)
      Storage.get({ key: 'phone' }).then((val) => {
        this.phone = val.value;

        if (this.password == null || this.phone == null) {
          this.router.navigateByUrl('/login', { replaceUrl: true });
        } else if (this.password == '' || this.phone == '') {
          this.router.navigateByUrl('/login', { replaceUrl: true });
        } else {
          this.getinfor(this.phone, this.password);

          setTimeout(() => this.getreport(), 7000);
          setTimeout(() => this.getpeople(), 7000);
         
          
          this.loadingbar();
          setTimeout(
            () => this.router.navigateByUrl('/tabs', { replaceUrl: true }),
            5000
          );
        }
      });
    });
  }

  async loadingbar() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: '載入中',
      duration: 7000,
      spinner: 'circular',
    });
    this.loading = await loading.present();
  }

  getinfor(phonewthcode, password) {
    this.CognitoSerive.getuserattribite(phonewthcode, password).then((data)=>{
      console.log('userdata',data);
      this.temp = data;
      Storage.get({ key: 'userdata' }).then((val) => {
       // var temp = JSON.parse(val.value);
  
        this.temp.forEach((data) => { 
          if (data.Name == 'custom:route') {
            //get routedata to call api
            this.arrrote = data.Value.split(',');
  
            this.arrrote.forEach((route) => {
              VehisericeService.zoneapi(route);
              VehisericeService.getrouteapi(route);
            });
          }
        });
       
      });
    });
    
  }

  getpeople() {
    VehisericeService.zonearr.forEach((arr) => {
      VehisericeService.peopleapi(arr);
    });

    setTimeout(() => this.getpeople(), 30000);
  } 

  getdate() {
    var rowdate = new Date().getTime();
    var today = new Date(rowdate - 86400000);

    if (today.getDate() < 10) {
      var day = '0' + JSON.stringify(today.getDate());
    } else {
      var day = JSON.stringify(today.getDate());
    }

    var month = today.getMonth() + 1;

    if (month < 10) {
      var fmonth = '0' + month;
    } else {
      var fmonth = JSON.stringify(month);
    }

    var year = today.getFullYear();
    this.today = year + '-' + fmonth + '-' + day;

    var yesterday = new Date(rowdate - 172800000);

    if (yesterday.getDate() < 10) {
      var yesday = '0' + JSON.stringify(yesterday.getDate());
    } else {
      var yesday = JSON.stringify(yesterday.getDate());
    }

    var yesmonth = yesterday.getMonth() + 1;

    if (yesmonth < 10) {
      var gmonth = '0' + yesmonth;
    } else {
      var gmonth = JSON.stringify(month);
    }

    var yesyear = yesterday.getFullYear();

    this.yesterday = yesyear + '-' + gmonth + '-' + yesday;
    Storage.remove({ key: 'report' + this.yesterday });
  }

  getreport() {
    this.getdate();
    for (const [key, value] of Object.entries(VehisericeService.routearr)) {
      VehisericeService.getreport(key, this.today);
      // this.arr = value
      // console.log(value);
      // this.arr.routes.forEach((route) => {
      //   VehisericeService.getreport(route, this.today);
      // });
    }
  }

  getsession(){
    RestfulService.restfulGet(this.sessionURL).subscribe(
      (session) => {
        console.log('session',session)
        this.session = session;
        Storage.set({key:'session',value: JSON.stringify(session)})
      })
  }
  // test(){
  //   Storage.get({ key: 'apikey' }).then((val) => {
  //     console.log('CognitoService.accessToken',val.value)
  //     RestfulService.cognitoGET('https://vcx6q715s8.execute-api.ap-east-1.amazonaws.com/Test/pets',val.value ).subscribe((data) => {
  //         console.log('splashTest',data)
  //       }) 
  //   })
  
    // RestfulService.cognitoGET('https://vcx6q715s8.execute-api.ap-east-1.amazonaws.com/Test/pets',CognitoService.accessToken ).subscribe((data) => {
    //   console.log('splashTest',data)
    // })
  //}
}
