import { Component, OnInit } from '@angular/core';
import { VehisericeService } from '../api/vehiserice.service';
import { Storage } from '@capacitor/storage';
import { PickerController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { RestfulService } from '../api/restful.service';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {
  routenum: any;
  reportresult;
  totalround: any;
  totalmile: any;
  totalvehi: any;
  returnval;
  vehifirst: any;
  showlast: any;
  showfirst = null;
  enterdate: string;
  loading: HTMLIonLoadingElement;
  today: string;
  yesterday: string;
  reportdata: any;
  routenum2: any;
  day: string;
  year: number;
  fmonth: string;
  yearpicker: any;
  monthpicker: any[];
  daypicker: any;

  constructor(
    private pickerCtrl: PickerController,
    private alertCtrl: AlertController,
    public loadingController: LoadingController,
    public http: HttpClient,
    private router: Router
  ) {}

  ngOnInit() {
    RestfulService.setHttp(this.http);
  }
  ionViewWillEnter() {
    this.getdate();
this.startprocess();
   
 
  }


  startprocess(){
    Storage.get({ key: 'nextreport' }).then((val) => {
    
      this.routenum2 = JSON.parse(val.value).readId;
      this.routenum = JSON.parse(val.value).id;
      Storage.get({ key: 'reportdisplay' }).then((val) => {
        this.reportresult = JSON.parse(val.value);
        if (!this.reportresult) {
          Storage.get({ key: 'report'+this.today}).then((val) => {
          
            this.reportdata = JSON.parse(val.value)
          
            this.reportdata.forEach(id => {
              if(id.id == this.routenum2 ){
    
    this.enterdate=id.date
    //Storage.set({ key: 'reportdate', value: JSON.stringify(this.enterdate) });
    this.returnval = id.data;
      this.returnval.sort((a, b) => {
        return b.TripInfo.length - a.TripInfo.length;
      });
    
      this.reportresult = this.returnval.map((data) => {
   
        return {
          rou: this.routenum,
          date: id.date,
          vid: data.vid,
          TripInfo: data.TripInfo,
          lc: Math.round(data.lc / 1000),
        };
      });
      this.totalvehi = this.reportresult.length;
      //     //this.totalround
    
      var triparr = this.reportresult.map((data) => {
        return data.TripInfo.length;
      });
    
      this.totalround = triparr.reduce((sum, trip) => {
        return sum + trip;
      });
    
  
      this.totalmile = this.reportresult.reduce((total, mile) => {
        return total + mile.lc;
      }, 0);
    
      this.findvan();
    
              }
            });
          })
       
  
          
       
        }else{
         
          this.totalvehi = this.reportresult.length;
          var triparr = this.reportresult.map((data) => {
            return data.TripInfo.length;
          });

          this.totalround = triparr.reduce((sum, trip) => {
            return sum + trip;
          });
        
          this.totalmile = this.reportresult.reduce((total, mile) => {
            return total + mile.lc;
          }, 0);

          Storage.get({ key: 'nextreport' }).then((val) => {
        
            
           
            Storage.get({ key: 'reportfirst' }).then((val) => {
              this.showfirst = JSON.parse(val.value);
              Storage.get({ key: 'reportlast' }).then((val) => {
                this.showlast = JSON.parse(val.value);
                Storage.get({ key: 'reportdate' }).then((val) => {
                  this.enterdate = JSON.parse(val.value);
                 
      
                 
                });
              });
            });
          });
        }

      });
  }) 
  }



  async openPicker() {
  
    const picker = await this.pickerCtrl.create({
      columns: [
        {
          name: 'year',
          options: this.yearpicker,
        },
        {
          name: 'month',
          options:this.monthpicker,
        },
        {
          name: 'day',
          options:this.daypicker,
        },
      ],
      buttons: [
        {
          text: '取消',
          role: 'cancel',
        },
        {
          text: '確定',
          handler: (value) => {
        
            this.enterdate =
              value.year.text + '-' + value.month.text + '-' + value.day.text;
         
            if (
              this.enterdate == value.year.text + '-' + '02-31' ||
              this.enterdate == value.year.text + '-' + '02-29' ||
              this.enterdate == value.year.text + '-' + '02-30' ||
              this.enterdate == value.year.text + '-' + '04-31' ||
              this.enterdate == value.year.text + '-' + '06-31' ||
              this.enterdate == value.year.text + '-' + '09-31' ||
              this.enterdate == value.year.text + '-' + '11-31'
            ) {
              this.alertcontroller('該月份沒有31號');
          
            } else {
              this.reportresult = '';
              this.showfirst = null;
             
              this.getreport(this.routenum2, this.enterdate);
            }
          },
        },
      ],
    });

    await picker.present();
  }

  getreport(id, date) {
    this.loadingbar();

    VehisericeService.getreport(id, date)
      .then((val) => {
        this.returnval = val;
        this.returnval.sort((a, b) => {
          return b.TripInfo.length - a.TripInfo.length;
        });

        this.reportresult = this.returnval.map((data) => {
          return {
            rou: this.routenum,
            date: date,
            vid: data.vid,
            TripInfo: data.TripInfo,
            lc: Math.round(data.lc / 1000),
          };
        });
        this.totalvehi = this.reportresult.length;
        //     //this.totalround

        var triparr = this.reportresult.map((data) => {
          return data.TripInfo.length;
        });

        this.totalround = triparr.reduce((sum, trip) => {
          return sum + trip;
        });

   

        this.totalmile = this.reportresult.reduce((total, mile) => {
          return total + mile.lc;
        }, 0);
        this.loading.dismiss();
        this.findvan();
      })
      .catch((err) => {
        this.alertcontroller('日期選擇錯誤 ,' + err);
        this.reportresult = null;
        this.showfirst = null;

        this.loading.dismiss();
      });
  }

  findvan() {
    this.vehifirst = this.reportresult.map((vehi) => {
      vehi.TripInfo.sort((a, b) => {
        return a.gt - b.gt;
      });

      return {
        vid: vehi.vid,
        first: vehi.TripInfo[0].gt,
        last: vehi.TripInfo[vehi.TripInfo.length - 1].gt,
      };
    });

    this.vehifirst.sort((a, b) => {
      return a.first - b.first;
    });
   
    this.showfirst = this.vehifirst[0];
    this.showfirst.first;
    var f_date = new Date(this.showfirst.first * 1000).toLocaleString('zh-HK', {
      timeZone: 'Asia/Hong_Kong',
      hour12: false,
      hour: '2-digit',
      minute: '2-digit',
    });


    this.showfirst.first = f_date;
    this.vehifirst.sort((a, b) => {
      return b.last - a.last;
    });
    this.showlast = this.vehifirst[0];
    var ehktime = new Date(this.showlast.last * 1000).toLocaleString('zh-HK', {
      timeZone: 'Asia/Hong_Kong',
      hour12: false,
      hour: '2-digit',
      minute: '2-digit',
    });
    //var l_date =new Date(hktime);
  
    // if(l_date.getHours()>9){
    //   var l_hours =JSON.stringify(l_date.getHours());
    // }else {
    //   var l_hours ="0"+l_date.getHours();
    // }

    // if(l_date.getMinutes()>9){
    //   var l_min =JSON.stringify(l_date.getMinutes());
    // }else {
    //   var l_min ="0"+l_date.getMinutes();
    // }
    this.showlast.last = ehktime;
    Storage.set({key: 'reportdisplay',value: JSON.stringify(this.reportresult)});
    Storage.set({ key: 'reportfirst', value: JSON.stringify(this.showfirst) });
    Storage.set({ key: 'reportlast', value: JSON.stringify(this.showlast) });
    Storage.set({ key: 'reportdate', value: JSON.stringify(this.enterdate) });

    //this.loading.dismiss();
  
  }

  async alertcontroller(title) {
    let alert = this.alertCtrl.create({
      header: title,

      buttons: ['OK'],
    });
    (await alert).present();
  }
  async loadingbar() {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: '載入中',
      duration: 60000,
      spinner: 'circular',
    });
    await this.loading.present();
  }

  nextpage(_item) {
  
    Storage.set({ key: 'reportdetail', value: JSON.stringify(_item) });
    this.router.navigateByUrl('/reportdetail', { replaceUrl: true });
  }
  getdate(){
    var rowdate = new Date().getTime();
    var today= new Date(rowdate-86400000)
  
    if(today.getDate()<10){
      this.day = "0"+JSON.stringify(today.getDate())
    }else{
      this.day = JSON.stringify(today.getDate())
    }
    
    var month = today.getMonth()+1
    
    if(month<10){
      this.fmonth = "0"+month;
    }else{
      this.fmonth = JSON.stringify(month);
    }
    
   this.year = today.getFullYear()
    this.today = this.year+"-"+ this.fmonth+"-"+ this.day
  
    var yesterday= new Date(rowdate-172800000)
    
    if(yesterday.getDate()<10){
      var yesday = "0"+JSON.stringify(yesterday.getDate())
    }else{
      var yesday = JSON.stringify(yesterday.getDate())
    }
    
    var yesmonth = yesterday.getMonth()+1
    
    if(yesmonth<10){
      var gmonth = "0"+yesmonth;
    }else{
      var gmonth = JSON.stringify(month);
    }
    
    var yesyear = yesterday.getFullYear()
  
     this.yesterday = yesyear+"-"+ gmonth+"-"+ yesday
     Storage.remove({ key: 'report'+ this.yesterday});

     this.yearpicker=[];
     this.monthpicker = [];
     this.daypicker = [];

for (let y = 0; y < 2; y++) {
  this.yearpicker.push({ text: this.year-y,
    value: this.year-y})
}

var mon = Number(this.fmonth);
for (let m = 0; m < 12; m++) {
 
  if(mon != 0){
    if(mon<10){
      this.monthpicker.push({ text: "0"+mon,
        value: "0"+mon})
    }else{
      this.monthpicker.push({ text: JSON.stringify(mon),
      value: JSON.stringify(mon)})
    }
   
    mon = mon-1
    console.log(this.monthpicker)
  }else {
    mon= mon+12
    this.monthpicker.push({ text:mon,
      value: mon})
    console.log(this.monthpicker)
    mon = mon-1
  }
}
  var da = Number(this.day)

 for (let d = 0; d < 31; d++) {
  
 if(da != 0){
  console.log(da)
  if(da<10){
    this.daypicker.push({ text: "0"+da,
      value: "0"+da})
  }else{
    this.daypicker.push({ text: JSON.stringify(da),
    value: JSON.stringify(da)})
  }
  
 da = da-1
 }else if(da == 0){
  da =da+31
  this.daypicker.push({ text: JSON.stringify(da),
      value: JSON.stringify(da)})
  da = da-1
 }
  
 }




}
}

