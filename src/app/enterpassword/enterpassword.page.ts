import { Component, OnInit } from '@angular/core';
import { Storage } from '@capacitor/storage';
import { CognitoService} from "../api/cognito.service";
import { Router, NavigationExtras } from "@angular/router";
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-enterpassword',
  templateUrl: './enterpassword.page.html',
  styleUrls: ['./enterpassword.page.scss'],
})
export class EnterpasswordPage implements OnInit {
  phone: any;
  verified: any;
  password: any;
  mess: string;

  constructor(public CognitoSerive:CognitoService,private router: Router,private alertController: AlertController) { }

  ngOnInit() {
    Storage.get({ key: 'phone' }).then((val) => {
      this.phone =val.value;
    })
   
  }

confrimPW(){
  console.log("verifify",this.phone,this.verified,this.password)
this.CognitoSerive.ftpwconfirm(this.phone,this.verified,this.password).then(
  res => {
    console.log(res);
    var mess = "成功更改密碼"
this.alertbox(mess);
    this.router.navigateByUrl('login', { replaceUrl: true });
  },
  err => {
    console.log(err);
  }
);
}

  async alertbox(mess){
const alert = await this.alertController.create({
      header: mess,
      buttons: [
        
        {
          text: 'OK',
          role: 'confirm',
          handler: () => {
            this.mess = mess;
          },
        },
      ],
    });

    await alert.present();

}


}

