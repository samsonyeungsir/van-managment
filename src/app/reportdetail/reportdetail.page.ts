import { Component, OnInit } from '@angular/core';
import { Storage } from '@capacitor/storage';

@Component({
  selector: 'app-reportdetail',
  templateUrl: './reportdetail.page.html',
  styleUrls: ['./reportdetail.page.scss'],
})
export class ReportdetailPage implements OnInit {
  detaildata: any;
  route: any;
  plate: any;
  date: any;
  dispalyarray: any;

  constructor() { }

  ngOnInit() {
  this.getdata();
  }  

getdata(){
  Storage.get({ key: 'reportdetail' }).then((val) => {
    this.detaildata = JSON.parse(val.value);
  console.log(this.detaildata)
  this.route = this.detaildata.rou;
  this.plate = this.detaildata.vid;
  this.date = this.detaildata.date;
  console.log('detalreport',this.route,this.plate,this.date )
  this.detaildata.TripInfo.sort((a, b) => {
   
    return a.gt- b.gt;
  });
this.dispalyarray =this.detaildata.TripInfo.map(data=>{
  var start = new Date(data.gt*1000).toLocaleString('zh-HK',{ timeZone: 'Asia/Hong_Kong',hour12: false,hour: '2-digit', minute:'2-digit' });
  var end = new Date(data.end_gt*1000).toLocaleString('zh-HK',{ timeZone: 'Asia/Hong_Kong',hour12: false,hour: '2-digit', minute:'2-digit' });
  var total  = Math.round((data.end_gt-data.gt)/60)
  var mile = (data.trip_milage/1000).toFixed(2);
  console.log( {str:start,end:end,time:total,mile:mile})
return {str:start,end:end,time:total,mile:mile}

})
  })
}



}
