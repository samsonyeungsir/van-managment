import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VanvideoPageRoutingModule } from './vanvideo-routing.module';

import { VanvideoPage } from './vanvideo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VanvideoPageRoutingModule
  ],
  declarations: [VanvideoPage]
})
export class VanvideoPageModule {}
