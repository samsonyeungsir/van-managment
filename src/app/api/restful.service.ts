import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class RestfulService {
  public static http: HttpClient;
  //static URLink = 'http://dev.taxieco.org/'

  constructor(public http: HttpClient) { }
  public static setHttp(_http) {
    this.http = _http;
  }
  public static restfulGet(URL: string) {
    return this.http.get(URL);
  }
     public static restfulPost(URL: string, postData,) {
   
  
      let newheader = new HttpHeaders();
      newheader.append('content-type', 'application/json');
      newheader.append('Access-Control-Allow-Origin', '*');
      newheader.append('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept');
      newheader.append('Accept', 'application/json');
      newheader.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    return this.http.post(URL, postData, {
        headers: newheader
  
      });
     }
     public static cognitoGET(URL: string,key) {
   
  

      let newheader = new HttpHeaders(
        {
          
          'Access-Control-Allow-Headers':'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods':' GET, OPTIONS',
          'Accept': 'application/json, text/plain, */*',
          'Authorization':key,
          // 'X-Amz-Date':'',
          // 'X-Api-Key':key,
          // 'X-Amz-Security-Token':''
        }
      );
      // newheader.set('Access-Control-Allow-Headers', 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token');
     
     
      // newheader.append('content-type', 'application/json')
      // newheader.append('Access-Control-Allow-Origin', '*')
  

      // newheader.append('Accept', 'application/json')
  
      // newheader.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT')
     

    
      return this.http.get(URL
       , { headers: newheader}
        );
      
     }
}