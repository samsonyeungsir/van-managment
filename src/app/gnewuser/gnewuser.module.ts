import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GnewuserPageRoutingModule } from './gnewuser-routing.module';

import { GnewuserPage } from './gnewuser.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GnewuserPageRoutingModule
  ],
  declarations: [GnewuserPage]
})
export class GnewuserPageModule {}
