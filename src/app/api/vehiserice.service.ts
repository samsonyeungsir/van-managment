import { Injectable } from '@angular/core';
import { RestfulService } from '../api/restful.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root',
})
export class VehisericeService {
  public static http: HttpClient;
  // public static routeURL = 'https://bmmzpyd2g8.execute-api.ap-east-1.amazonaws.com/Prod/hello/';
  public static routeURL = 'https://gmb-data.s3.ap-east-1.amazonaws.com/';
  public static subfixURL = '.json';
  public static zoneURL =
    'https://s3ectjzjx5.execute-api.ap-east-1.amazonaws.com/Prod/hello/?gmbRoute=';
  public static routedata: Object;
  public static routearray: any;
  public static reportURL =
    'https://r6d97sd4ti.execute-api.ap-east-1.amazonaws.com/Prod/hello/?gmbRoute=';
  public static dateURL = '&reportDate=';
  public static report;
  static vehiarr: any[];
  static getsessionURL = 'displaycallsessionid.php';
  static peoplecountarr: any[];
  static URLink: string;
  static session;
  public static getpeopleURL =
    'https://usg9eftc7b.execute-api.ap-east-1.amazonaws.com/Prod/classify_digit/?licensePlateNumber=';
  static peoplecount;
  public static vidURL = '&licensePlateNumber=';
  static setpeople;
  public static routearr;
  public static rawvehi;
  static people: any;
  static getpeopletime;
  static routearrzone;
  static zonearr;
  static vid;
  static getroute: any;
  static reportback: any;
  constructor(public http: HttpClient) {}

  public static getrouteapi(route) {
    this.routearr = {};

    RestfulService.restfulGet(this.routeURL + route + this.subfixURL).subscribe(
      (data) => {
        // this.routearr.push(data);
        this.routearr[route] = data;

        // console.log('this.routearr',data)
      }
    );
  }

  public static zoneapi(route) {
    this.zonearr = [];

    RestfulService.restfulGet(this.zoneURL + route).subscribe((data) => {
   console.log('vehidata',data)
      this.zonearr.push(data);
      
      this.getroute = data
     
        Storage.set({ key: 'zone' + route, value: JSON.stringify(data) });
    
    

     
      setTimeout(() => this.zoneapi(route), 60000);
      //VehisericeService.getpeoplecount(this.routearr)
    });
  }

  public static getreport(id, today) {
 
    this.report = [];
    return new Promise((resolved, reject) => {
      RestfulService.restfulGet(
        this.reportURL + id + this.dateURL + today
      ).subscribe((data) => {
        
        this.report.push({ date: today, id: id, data: data });
resolved(data)
        Storage.set({
          key: 'report' + today,
          value: JSON.stringify(this.report),
        });
       
      });
    });
  }

  public static getpeoplecount(routearr) {
    //Storage.set({ key: "routedetail",value:JSON.stringify(this.routearr)});

    this.rawvehi = [];

    Storage.get({ key: 'URLink' }).then((val) => {
      this.URLink = val.value;
      RestfulService.restfulGet(this.URLink + this.getsessionURL).subscribe(
        (session) => {
          this.session = session;
          // for (let i = 0; i < routearr.length; i++) {

          routearr.routes.forEach((routeid) => {
            routearr.zones.forEach((el) => {
              el.vehicles.forEach((vehi) => {
                this.rawvehi.push({
                  route: routeid.name,
                  vid: vehi.nm,
                  people: null,
                });
              });
            });
          });
        }
        //}
      );
    });

    // setTimeout(() => this.peopleapi(), 1000);

    //this.setpeople = setTimeout(() => this.getpeoplecount(routearr), 60000);
  }

  public static peopleapi(routearr) {
    this.rawvehi = [];
    routearr.zones.forEach((el) => {
      el.zoneInfo.forEach(infor => {
        infor.vehicles.forEach((vehi) => {
          RestfulService.restfulGet(this.getpeopleURL + vehi.nm).subscribe(
            (people) => {
              this.vid = people;
              Storage.set({
                key: 'people' + this.vid.licensePlateNumber,
                value: JSON.stringify(people),
              });
              this.rawvehi.push(people);
            }
          );
          // this.rawvehi.push({
          //   route: routeid.name,
          //   vid: vehi.nm,
          //   people: null,
          // });
        });
      });
      
    });

    //   RestfulService.restfulGet(
    //     this.getpeopleURL+ vid.vid
    //   ).subscribe((people) => {
    //     this.people = people;

    //     if (this.people.licensePlateNumber == vid.vid) {
    //       vid.people = this.people.Person;
    //     }
    //   });
    //});
  }
}
