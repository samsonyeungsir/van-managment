import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { RestfulService } from "../api/restful.service";
import { HttpClient } from "@angular/common/http";
import { ToastController } from "@ionic/angular";
import { Storage } from '@capacitor/storage';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {
username:string;
company:string;
  admin;
  userdata: any;
  constructor(private router: Router,
    public http: HttpClient,
    public toasterCtrl: ToastController) { }

  ngOnInit() {
 

  }
  ionViewWillEnter() { 

    Storage.get({key: 'user_name'}).then(val => {
      this.username = val.value;
          })
          Storage.get({key: 'phone'}).then(val => {
            this.company = val.value;
            console.log('this.company',this.company)
           
                })
                Storage.get({key: 'userdata'}).then(val => {
                  this.userdata = JSON.parse(val.value);
                  this.userdata.forEach(val=>{
                    if(val.Name == 'custom:admin'){
        this.admin= val.Value
        
                    
              
                    }
                    // if(val.Name == 'given_name'){
                    
                    //   this.company = val.Value
                                  
                            
                    //               }
                  })
                  
                  console.log('userdata', this.userdata);
                      })
  }
logout(){
  Storage.set({ key: "company", value:""});
  Storage.remove({
    key: "password",
  });
  Storage.remove({
    key: "phone",
  });
  this.router.navigateByUrl('/login', { replaceUrl: true });
}

changepassword(){

  this.router.navigateByUrl('forgetpassword', { replaceUrl: true });
}

newuser(){
  this.router.navigateByUrl('gnewuser', { replaceUrl: true });

}

}
