import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EnterpasswordPage } from './enterpassword.page';

const routes: Routes = [
  {
    path: '',
    component: EnterpasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnterpasswordPageRoutingModule {}
