import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EnterpasswordPageRoutingModule } from './enterpassword-routing.module';

import { EnterpasswordPage } from './enterpassword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnterpasswordPageRoutingModule
  ],
  declarations: [EnterpasswordPage]
})
export class EnterpasswordPageModule {}
