import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { RestfulService } from "../api/restful.service";
import { VehisericeService} from "../api/vehiserice.service";
import { HttpClient } from "@angular/common/http";
import { ToastController } from "@ionic/angular";
import { Storage } from '@capacitor/storage';
import { CognitoService} from "../api/cognito.service";
import { LoadingController } from '@ionic/angular';

import sha256 from 'crypto-js/sha256';
import CryptoJS from 'crypto-js';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public URLink = 'http://dev.taxieco.org/'
  public userdata = { username: "", password: "" };
  public username;
  public data;
  public JSON_RESULT;
  public SERVER_URL = "userlogin.php";
  public company_URL = "getcompanyname.php";
  public getVehiURL="callvehi.php";
  JSON_USER: any;
  public lang;
  public hashPW;
  routearr = [{ route: "60K", area: "NT" }]
  vehidata;

  vehiarr = [];
taxicolor = [];
companydata;
  phonewthcode: string;
  company: any;
  comdata: any;
  loginkey: any;
  password: any;


  constructor( private router: Router,
    public http: HttpClient,
    public toasterCtrl: ToastController,
    public CognitoSerive:CognitoService,
    public loadingController: LoadingController
    ) { }

  ngOnInit() {
    RestfulService.setHttp(this.http);
  }

  login(){
   // VehisericeService.getpeoplecount();
    this.phonewthcode = "+852"+this.userdata.username

    if (this.userdata.username != "" || this.userdata.password != ""){
      this.CognitoSerive.authenticate(this.phonewthcode, this.userdata.password)
      .then(res =>{
        console.log("login",res);
        Storage.set({ key: "loginkey",value:JSON.stringify(res)});
        Storage.set({ key: "phone",value:this.phonewthcode});
        Storage.set({ key: "password",value:this.userdata.password});
       
       this.CognitoSerive.getuserattribite(this.phonewthcode, this.userdata.password);
     
 setTimeout(() => this.getinfor(),1000);
  
   
      }, err =>{
        console.log(err);
        this.showToast("電話或密碼錯誤")
      });
    

       
      } else {
        this.showToast("請輸入電話或密碼")

}


  }

  async loadingbar(){

    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: '載入中',
      duration: 5000,
      spinner: "circular"
    });
    await loading.present();
  }

  getinfor(){
    Storage.get({ key: 'password' }).then((val) => {

      this.password = val.value
      console.log(this.password)
      this.CognitoSerive.getuserattribite(this.phonewthcode, this.password)
    })
      
    
    setTimeout(() => this.router.navigateByUrl('/splash', { replaceUrl: true }), 100);
  }
public async showToast(messageShow) {
  (
    await this.toasterCtrl.create({ 
      message: messageShow, 
      duration: 3000,
      position: "top"
    })
  ).present();
} 
gotoNextField(nextElement){
nextElement.setFocus();
} //end

forpw(){
  this.router.navigateByUrl('/forgetpassword', { replaceUrl: true });
}




}
