import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.van.manage.www',
  appName: 'vanproject',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
