import { Injectable } from '@angular/core';
import * as AWSCognito from "amazon-cognito-identity-js";
import { environment } from '../../environments/environment';
import * as AWS from 'aws-sdk/global';
import { Storage } from '@capacitor/storage';
import { Router, NavigationExtras } from "@angular/router";
import { ToastController } from "@ionic/angular";
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CognitoService {
  identitypool = "ap-southeast-1:cb9e2aab-a819-4a2f-8441-e52c21e30a38"
  _POOL_DATA = environment.cognito_data;
  public static returnresult =""
  public static accessToken
  constructor(public router: Router,public toasterCtrl: ToastController,private alertCtrl: AlertController,) { }

  authenticate(phone, password) {
    return new Promise((resolved, reject) => {
      const userPool = new AWSCognito.CognitoUserPool(this._POOL_DATA);

      const authDetails = new AWSCognito.AuthenticationDetails({
        Username: phone,
        Password: password
      });

      const cognitoUser = new AWSCognito.CognitoUser({
        Username: phone,
        Pool: userPool
      });

      cognitoUser.authenticateUser(authDetails, {
        onSuccess: result => {
              resolved(result);
              var accessToken = result.getAccessToken().getJwtToken();
              console.log('result',result)
            },
            onFailure: err => {
              reject(err);
            },
            newPasswordRequired: (userAttributes,requiredAttributes) => {
             //userAttributes.phone_number = phone;
                 delete userAttributes.phone_number;
      console.log("userAttributes",userAttributes,requiredAttributes)
               
                 cognitoUser.completeNewPasswordChallenge(password, {}, {

        onSuccess: function(result) {
          console.log('completeNewPassword',result)
        },
                  onFailure: function(error) {
                    reject(error);
                  }
                 })
        
           }
         
      })
     
      
    });
  }
getuserattribite(phone,password){
  return new Promise((resolved, reject) => {
    var authenticationData = {
      Username: phone,
      Password: password,
    };
    var authenticationDetails = new  AWSCognito.AuthenticationDetails(
      authenticationData
    );
   
    var userPool = new  AWSCognito.CognitoUserPool(this._POOL_DATA);
    var userData = {
      Username: phone,
      Pool: userPool,
    };
    var cognitoUser = new AWSCognito.CognitoUser(userData);

    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function(result) {
        Storage.set({ key: 'apikey', value: result.getIdToken().getJwtToken() });
      var accessToken = result.getAccessToken().getJwtToken();
    console.log('result',result)
    console.log('result',result.getIdToken().getJwtToken())
        //POTENTIAL: Region needs to be set if not already set previously elsewhere.
        AWS.config.region = "ap-southeast-1";
    
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          
          IdentityPoolId:"ap-southeast-1:cb9e2aab-a819-4a2f-8441-e52c21e30a38", 
          
          // your identity pool id here
          Logins: {
            
            // Change the key below according to the specific region your user pool is in.
            'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_ZXCSF10y5': result
              .getIdToken()
              .getJwtToken(),
          },
        });
    
       // refreshes credentials using AWS.CognitoIdentity.getCredentialsForIdentity()
       ( < AWS.CognitoIdentityCredentials > AWS.config.credentials).refresh(error => {
          if (error) {
            
            console.log(error);
          } else {
            // Instantiate aws sdk service objects now that the credentials have been updated.
            // example: var s3 = new AWS.S3();
            console.log('Successfully logged!');
            cognitoUser.getUserAttributes(function(err, result) {
              if (err) {
                console.log(JSON.stringify(err))
                //alert(err.message || JSON.stringify(err));
                return;
              }
              resolved(result);
              console.log("attresult",result)
            //this.returnresult = result
            Storage.set({ key: "userdata", value:JSON.stringify(result)});
              // for (var i = 0; i < result.length; i++) {
              //   console.log(
              //     'attribute ' + result[i].getName() + ' has value ' + result[i].getValue()
              //   );
              // }
              
            });
          }
        });
      },
    
      onFailure: async function(err) {
        reject(err);
        console.log('err.message',err)
       alert(err.message || JSON.stringify(err));
    //      this.alertCtrl.create({
    //             header:err.message,
          
    //             buttons: [ {
    //               text: 'OK',
    //               role: 'confirm',
    //               handler: () => {
                   
                  navigator['app'].exitApp();
    //               }}],
    //           }).present();
              
         
          
    },
   });


  })
 
 
  
 
}


forgetpassword(phone){
  const userPool = new AWSCognito.CognitoUserPool(this._POOL_DATA);

  const cognitoUser = new AWSCognito.CognitoUser({
    Username:phone,
    Pool: userPool
  });
  console.log("forget")
  this.nextpage();
  cognitoUser.forgotPassword({
  
    onSuccess: function(data) {
      // successfully initiated reset password request
      console.log('CodeDeliveryData from forgotPassword: ' + data);
    },
    onFailure: function(err) {
      alert('02'+err.message || JSON.stringify(err));
    },
  
    //Optional automatic callback
    inputVerificationCode: function() {
      // var verificationCode = prompt('Please input verification code ', '');
      //       var newPassword = prompt('Enter new password ', '');
     
  
   
    },
  });

}

ftpwconfirm(phone,verificationCode,newPassword){
  return new Promise((resolved, reject) => {
    const userPool = new AWSCognito.CognitoUserPool(this._POOL_DATA);

    const cognitoUser = new AWSCognito.CognitoUser({
      Username:phone,
      Pool: userPool
    });
    cognitoUser.confirmPassword(verificationCode, newPassword, {
      async onSuccess() {
        resolved('Password confirmed!');
   
       
      },
      async onFailure(err) {
        reject(err);
        
     
      },

    })

  })

}
 nextpage(){
 
  this.router.navigateByUrl('/enterpassword', { replaceUrl: true });
 }

 signUp(phone, password,route,admin) {
  return new Promise((resolved, reject) => {
    const userPool = new AWSCognito.CognitoUserPool(this._POOL_DATA);
    Storage.get({ key: 'company' }).then((val) => {
      var company = val.value
      console.log("cognito com",company)
      let userAttribute = [];
      var attributePhoneNumber = new AWSCognito.CognitoUserAttribute({ Name: "phone_number", Value: phone });
      var attributegivenname = new AWSCognito.CognitoUserAttribute({ Name: "given_name", Value: company });
      var attributeroutename = new AWSCognito.CognitoUserAttribute({ Name: "custom:route", Value: route });
      var attributeadminname = new AWSCognito.CognitoUserAttribute({ Name: "custom:admin", Value: admin });
      userAttribute.push (attributePhoneNumber);
      userAttribute.push (attributegivenname);
      userAttribute.push (attributeroutename);
      userAttribute.push (attributeadminname);
      userPool.signUp(phone, password, userAttribute, null, function(err, result) {
        if (err) {
          reject(err);
        } else {
          resolved(result);
        }
      });
    })
  });
}

resendsms(phone){
console.log(phone)
const userPool = new AWSCognito.CognitoUserPool(this._POOL_DATA);

const cognitoUser = new AWSCognito.CognitoUser({
  Username:phone,
  Pool: userPool
});
cognitoUser.resendConfirmationCode(function(err, result) {
  if (err) {
    alert('03'+err.message || JSON.stringify(err));
    return;
  }
  console.log('call result: ' + result);
});

}


confirmUser(verificationCode, phone) {
  return new Promise((resolved, reject) => {
    const userPool = new AWSCognito.CognitoUserPool(this._POOL_DATA);

    const cognitoUser = new AWSCognito.CognitoUser({
      Username:phone,
      Pool: userPool
    });

    cognitoUser.confirmRegistration(verificationCode, true, async function(err, result) {
      if (err) {
        reject(err);
        
        
      } else {
        resolved(result); 
       
      }
    });
  });
}

}
