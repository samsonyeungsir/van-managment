import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router, NavigationExtras } from "@angular/router";
import { CognitoService} from "../api/cognito.service";
import { Storage } from '@capacitor/storage';
@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.page.html',
  styleUrls: ['./forgetpassword.page.scss'],
})
export class ForgetpasswordPage implements OnInit {
phone;
  constructor(private platform: Platform,private router: Router,public CognitoSerive:CognitoService) { }

  ngOnInit() {
    this.platform.backButton.subscribeWithPriority(10, () => {
      console.log('Handler was called!');
      this.router.navigateByUrl('/login', { replaceUrl: true });
    });
  }
  forgetpw(){
    var codephone = "+852"+this.phone
    Storage.set({ key: "phone", value: codephone });
    console.log(codephone)
    this.CognitoSerive.forgetpassword(codephone)
  
  }
}
