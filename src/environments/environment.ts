// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey:"AIzaSyAz_SVHrpWd8D3MD5nMuxYUm9jh03NNGoY",
  cognito_data :{
    UserPoolId: "ap-southeast-1_ZXCSF10y5",
    ClientId: "7kf1icv42b8tecf9cdid16auo4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
