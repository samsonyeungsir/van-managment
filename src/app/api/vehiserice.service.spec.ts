import { TestBed } from '@angular/core/testing';

import { VehisericeService } from './vehiserice.service';

describe('VehisericeService', () => {
  let service: VehisericeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VehisericeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
