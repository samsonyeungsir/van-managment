import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CognitoService} from "../api/cognito.service";
import { AlertController } from "@ionic/angular";

@Component({
  selector: 'app-gnewuser',
  templateUrl: './gnewuser.page.html',
  styleUrls: ['./gnewuser.page.scss'],
})
export class GnewuserPage implements OnInit {


  phone: string;
  phonewthcode;
  password: string;
route;
admin;
  comroute: any;
  constructor(public alertController: AlertController,
    public router: Router,
    public CognitoService: CognitoService) { }

  ngOnInit() {
  }

  async register() {
    
    this.phonewthcode = "+852"+this.phone
    this.comroute = this.route
    console.log(this.comroute,this.admin);
    this.CognitoService.signUp( this.phonewthcode, this.password,this.comroute,this.admin).then(
      res => {
        localStorage.setItem('phone', this.phonewthcode);  
        console.log("res",res)
        this.router.navigateByUrl('/confirm');
      },
      err => {
        console.log(err);
        this.presentAlert(err["message"]);
      }
    );
  }

  async presentAlert(alert_message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'error',
      message: alert_message,
      buttons: ['OK']
    });

    await alert.present();
  }

  radioSelect(event){
console.log(event)

  }


}

