import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@capacitor/storage';
import { HttpClient } from '@angular/common/http';
import { RestfulService } from '../api/restful.service';
import { VehisericeService } from '../api/vehiserice.service';
import { Router, NavigationExtras } from '@angular/router';
import { PopoverController, Platform, LoadingController } from '@ionic/angular';

declare var google;
@Component({
  selector: 'app-routedetail',
  templateUrl: './routedetail.page.html',
  styleUrls: ['./routedetail.page.scss'],
})
export class RoutedetailPage implements OnInit {
  routedata;
  stationname: any;
  routenum: any;
  zone;
  station;
  cardheight = 200;
  time: string;
  vehijam = [];
  zoneURL =
    'https://s3ectjzjx5.execute-api.ap-east-1.amazonaws.com/Prod/hello/?gmbRoute=';
  routedatatime;
  currenttimetimer;
  pageinfor: any;
  routedataarr: any;
  zonedisplay;
  getroutetime: NodeJS.Timeout;
  stationpath = [];
  patharray = [];
  loading;
  zone2: any;
  getzone;
  peopledata: any;
  onPauseSubscription: any;
  onResumeSubscription: any;
  api;

  constructor(
    RestfulService: RestfulService,
    private router: Router,
    public http: HttpClient,
    public popoverController: PopoverController,
    public loadingController: LoadingController,
    private platform: Platform
  ) { }

  ngOnInit() {

    RestfulService.setHttp(this.http);
    // this.loadingbar();
    this.platform.ready().then(() => {
      this.onPauseSubscription = this.platform.pause.subscribe(() => {
        // do stuff
        this.ionViewDidLeave();

      });

      this.onResumeSubscription = this.platform.resume.subscribe(() => {
        // do stuff
        this.ionViewWillEnter();

      });
    });
  }

  ionViewWillEnter() {

    this.getroutedata();
    this.currenttime();
    //this.getpeople();

    setTimeout(() => this.getzonedata(), 500);
    //VehisericeService.getrouteapi();
  }

  ionViewDidLeave() {

    clearTimeout(this.currenttimetimer);
    clearTimeout(this.routedatatime);
    clearTimeout(this.getzone);
    //VehisericeService.routearr= [];
    //VehisericeService.getrouteapi();
  }
  ionViewDidEnter() {

  }

  currenttime() {
    var currentdate = new Date();
    if (currentdate.getHours() < 10) {
      var hour = '0' + currentdate.getHours();
    } else {
      var hour = JSON.stringify(currentdate.getHours());
    }
    if (currentdate.getMinutes() < 10) {
      var min = '0' + currentdate.getMinutes();
    } else {
      var min = JSON.stringify(currentdate.getMinutes());
    }
    var datetime = hour + ':' + min;
    this.time = datetime;
    min;

    this.currenttimetimer = setTimeout(() => this.currenttime(), 30000);
  }

  getvideo(_item) {
    Storage.set({ key: 'vehidata', value: JSON.stringify(_item) });
    this.router.navigateByUrl('/vanvideo', { replaceUrl: true });
  }

  refresh(ev) {
    this.zone = [];
    clearTimeout(this.currenttimetimer);
    clearTimeout(this.routedatatime);
    clearTimeout(this.getroutetime);
    setTimeout(() => {
      this.ionViewWillEnter();

      ev.target.complete();
    }, 3000);
  }
  //routearr
  getroutedata() {
    this.station = [];
    Storage.get({ key: 'nextpage' }).then((val) => {
      this.pageinfor = JSON.parse(val.value);

      for (const [key, value] of Object.entries(VehisericeService.routearr)) {
        this.api = value;
        this.api.routes.forEach((route) => {
          if (route.name == this.pageinfor.id) {
            this.zonedisplay = this.api;
            console.log('this.zonedisplay', this.zonedisplay);
            this.stationname = route.long_name.split(/[a-zA-Z0-9]{1,}/gm)[0];
            this.routenum = route.name;
            route.patterns.forEach((stop) => {
              stop.stop_points.forEach((point, i) => {
                if (i != 0) {
                  var input =
                    (point.path_index - stop.stop_points[i - 1].path_index) / 3;
                } else {
                  var input = 0;
                }
                this.station.push({
                  station: this.zonedisplay.stops[point.id],
                  path: point.path_index,
                  dist: input,
                });
              });
            });
          }
        });
      }
console.log('this.zonedisplay',this.zonedisplay)
      this.zone = this.zonedisplay.zones.map((zone) => {

        this.vehijam = [];
        zone.vehicles.sort((a, b) => {
          return a.cloestPathPoint - b.cloestPathPoint;
        });

        zone.vehicles.forEach((vid) => {
          vid.people = '0%';
          if (vid.status == 1 && vid.currentACCStatusOn == 1) {
            this.vehijam.push(vid.status);
          }
        });
        if (zone.name != '不提供服務') {
          if (this.vehijam.length >= 2) {
            return {
              name: zone.name,
              vid: zone.vehicles,
              stn: [],
              zonestartstation: zone.zonestartstation,
              zonestopstation: zone.zonestopstation,
              cloestPathPoint: zone.cloestPathPoint,
              zonecolor: 1,
              diplay: zone.display,
              path: this.station[zone.zonestartstation - 1].path,
            };
          } else {
            return {
              name: zone.name,
              vid: zone.vehicles,
              stn: [],

              zonestartstation: zone.zonestartstation,
              zonestopstation: zone.zonestopstation,
              zonecolor: 0,
              diplay: zone.display,
              path: this.station[zone.zonestartstation - 1].path,
            };
          }
        } else {
          return { name: zone.name, vid: zone.vehicles };
        }
      });
      this.zone.forEach((stn) => {
        for (let i = stn.zonestartstation - 1; i < stn.zonestopstation; i++) {
          const REGEX_CHINESE =
            /[\u4e00-\u9fff]|[\u3400-\u4dbf]|[\u{20000}-\u{2a6df}]|[\u{2a700}-\u{2b73f}]|[\u{2b740}-\u{2b81f}]|[\u{2b820}-\u{2ceaf}]|[\uf900-\ufaff]|[\u3300-\u33ff]|[\ufe30-\ufe4f]|[\uf900-\ufaff]|[\u{2f800}-\u{2fa1f}]/u;
          var splitStr = this.station[i].station.name.split('');
          const chiWords = splitStr.filter((string) =>
            REGEX_CHINESE.test(string)
          );
          const lastChiWord = chiWords.pop();
          var teststr = this.station[i].station.name;
          var finalname = {
            name: teststr.substring(
              0,
              teststr.indexOf(lastChiWord) + lastChiWord.length + 1
            ),
            dist: this.station[i].dist,
          };
          stn.stn.push(finalname);
        }
        stn.vid.forEach((vid) => {
          //           var math =
          //             ((Math.log(vid.cloestPathPoint) - Math.log(stn.path)) /
          //               Math.log(stn.path)) *
          //             1000;
          // console.log('math',vid,math,vid.cloestPathPoint,stn.path);
          //           if (math < 0) {
          //             vid.dis = 0;
          //           } else {
          //             vid.dis = math;
          //           }
        });
      });
    });
    setTimeout(() => console.log('zone', this.zone), 500);
  }

  getzonedata() {
    Storage.get({ key: 'zone' + this.pageinfor.readID }).then((val) => {

      JSON.parse(val.value).zones.forEach((zdata) => {
        console.log('zdata',zdata);
        
        this.zone2 = zdata.zoneInfo.map(zone => {
          this.vehijam = [];


          zone.vehicles.sort((a, b) => {
            return a.cloestPathPoint - b.cloestPathPoint;
          });
          zone.vehicles.forEach((vid) => {

            vid.people = '0%';
            if (vid.status == 1 && vid.currentACCStatusOn == 1) {

              this.vehijam.push(vid.status);
            }
          });
          if (zone.name != '不提供服務') {
            return {
              name: zone.name,
              vid: zone.vehicles,
              stn: [],
              zonestartstation: zone.zonestartstation,
              cloestPathPoint: zone.cloestPathPoint,
              zonestopstation: zone.zonestopstation,
              zonecolor: (this.vehijam.length >= 2),
              diplay: zone.display,
              path: this.station[zone.zonestartstation - 1].path,
            };

            // if (this.vehijam.length >= 2) {

            //   return {
            //     name: zone.name,
            //     vid: zone.vehicles,
            //     stn: [],
            //     zonestartstation: zone.zonestartstation,
            //     cloestPathPoint: zone.cloestPathPoint,
            //     zonestopstation: zone.zonestopstation,
            //     zonecolor: 1,
            //     diplay: zone.display,
            //     path: this.station[zone.zonestartstation - 1].path,
            //   };
            // } else {
            //   return {
            //     name: zone.name,
            //     vid: zone.vehicles,
            //     stn: [],
            //     cloestPathPoint: zone.cloestPathPoint,
            //     zonestartstation: zone.zonestartstation,
            //     zonestopstation: zone.zonestopstation,
            //     zonecolor: 0,
            //     diplay: zone.display,
            //     path: this.station[zone.zonestartstation - 1].path,
            //   };
            // }
          } else {
            return { name: zone.name, vid: zone.vehicles };
          }
        })

      });

      this.zone2.forEach((stn) => {

        for (let i = stn.zonestartstation - 1; i < stn.zonestopstation; i++) {
          const REGEX_CHINESE =
            /[\u4e00-\u9fff]|[\u3400-\u4dbf]|[\u{20000}-\u{2a6df}]|[\u{2a700}-\u{2b73f}]|[\u{2b740}-\u{2b81f}]|[\u{2b820}-\u{2ceaf}]|[\uf900-\ufaff]|[\u3300-\u33ff]|[\ufe30-\ufe4f]|[\uf900-\ufaff]|[\u{2f800}-\u{2fa1f}]/u;
          var splitStr = this.station[i].station.name.split('');
          const chiWords = splitStr.filter((string) =>
            REGEX_CHINESE.test(string)
          ); 
          const lastChiWord = chiWords.pop();
          var teststr = this.station[i].station.name;
          var finalname = {
            name: teststr.substring(
              0,
              teststr.indexOf(lastChiWord) + lastChiWord.length + 1
            ),
            dist: this.station[i].dist,
          };
          stn.stn.push(finalname);
        }

        stn.vid.forEach((vid) => {
          Storage.get({ key: 'people' + vid.nm }).then((val) => {
            this.peopledata = JSON.parse(val.value);

            if (this.peopledata.licensePlateNumber == vid.nm) {
              if (this.peopledata.Person >= 15) {
                vid.people = '100%';
              } else if (
                this.peopledata.Person > 12 &&
                this.peopledata.Person < 15
              ) {
                vid.people = '75%';
              } else if (
                this.peopledata.Person > 8 &&
                this.peopledata.Person <= 12
              ) {
                vid.people = '50%';
              }
              if (this.peopledata.Person > 4 && this.peopledata.Person <= 8) {
                vid.people = '25%';
              } else if (this.peopledata.Person < 4) {
                vid.people = '0%';
              }
            }
          });

          var math =
            ((Math.log(vid.cloestPathPoint) - Math.log(stn.path)) /
              Math.log(stn.path)) *
            500;

          if (math < 0) {
            vid.dis = 0;
          } else {
            vid.dis = math;
          }
        });
      });
    });

    setTimeout(() => (this.zone = this.zone2), 500);

    this.getzone = setTimeout(() => this.getzonedata(), 30000);
  }

  async loadingbar() {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: '載入中',
      //duration: 4000,
      spinner: 'circular',
    });
    await this.loading.present();
  }

  getpeople() {
    this.zonedisplay = [];

    Storage.get({ key: 'nextpage' }).then((val) => {
      this.pageinfor = JSON.parse(val.value);

      VehisericeService.rawvehi.forEach((plate) => {
        this.zone.forEach((zone) => {
          zone.vid.sort((a, b) => {
            return a.cloestPathPoint - b.cloestPathPoint;
          });
          //this.zonedisplay =

          zone.vid.forEach((vid) => {
            if (vid.nm == plate.vid) {
              if (plate.people > 4) {
                //vid.dis = ((arr[0].cloestPathPoint-zone.path)/zone.path)*100
                vid.people = '25%';
              } else if (plate.people > 8) {
                // vid.dis =((arr[0].cloestPathPoint-zone.path)/zone.path)*100

                vid.people = '50%';
              } else if (plate.people > 12) {
                //vid.dis = ((arr[0].cloestPathPoint-zone.path)/zone.path)*100
                vid.people = '75%';
              } else if (plate.people > 15) {
                //vid.dis = ((arr[0].cloestPathPoint-zone.path)/zone.path)*100
                vid.people = '100%';
              } else {
                // vid.dis = ((arr[0].cloestPathPoint-zone.path)/zone.path)*100
                vid.people = '0%';
              }
            }
          });
        });
      });
    });

    this.routedatatime = setTimeout(() => this.getpeople(), 30000);
  }
}
