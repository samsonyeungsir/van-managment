import { Component, OnInit } from '@angular/core';
import { Storage } from '@capacitor/storage';
import { DomSanitizer,SafeResourceUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-vanvideo',
  templateUrl: './vanvideo.page.html',
  styleUrls: ['./vanvideo.page.scss'],
})
export class VanvideoPage implements OnInit {

videoURL ="http://apps.taxieco.org/808gps/open/player/video.html?lang=en&channel=1"
vehiURL = '&vehiIdno='
sessionURL = '&jsession='

vehi;

  video:SafeResourceUrl;
  vehiarr: any;
  videoarr: any;
  unsafev: any;
  videopre: any;
  videosub: any;
  plate: any;
  session;

  constructor(private sanitizer: DomSanitizer) {
    this.URL("");
   }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
   this.getSafeUrl();
   }

public getSafeUrl() {
  Storage.get({ key: 'session' }).then((val) => {
    this.session = JSON.parse(val.value).JSESSIONID
  Storage.get({ key: 'vehidata' }).then((val) => {
      this.vehi = JSON.parse(val.value)
      this.plate = this.vehi.nm
      var link = this.videoURL+ this.sessionURL+this.session+this.vehiURL+this.vehi.nm
      console.log(link)
      this.URL(link);
    })
  })
     //this.URL();
    console.log(this.vehi)
       
     
 

    }
    URL(link){
      this.video = this.sanitizer.bypassSecurityTrustResourceUrl(link);
    }
}
