import { Component } from '@angular/core';
import { RestfulService } from '../api/restful.service';

import { Storage } from '@capacitor/storage';
import { VehisericeService } from '../api/vehiserice.service';
import { Router, NavigationExtras } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
  reportresult;
  routedata: any;
  stationname: any;
  route;
  today: any;
  routetri: any[];
  routearr: any;
  data;

  constructor(
    private router: Router,
    public loadingController: LoadingController,
    public http: HttpClient
  ) {}
  ngOnInit() {
    RestfulService.setHttp(this.http);
  }

  ionViewWillEnter() {
    this.getdate();
    this.getinfor();
  }
  ionViewDidEnter() {
    Storage.set({ key: 'reportdisplay', value: null });
    Storage.set({ key: 'reportfirst', value: null });
    Storage.set({ key: 'reportlast', value: null });
    Storage.set({ key: 'reportdate', value: null });
  }

  getroute(_item) {
 
    Storage.set({ key: 'nextreport', value: JSON.stringify(_item) });
    this.router.navigateByUrl('/report', { replaceUrl: true });

    //this.loadingbar();
  }

  getdate() {
    var today = new Date();
    if (today.getDate() < 10) {
      var day = '0' + today.getDate();
    } else {
      var day = JSON.stringify(today.getDate());
    }

    var month = today.getMonth() + 1;

    if (month < 10) {
      var fmonth = '0' + month;
    } else {
      var fmonth = JSON.stringify(month);
    }

    var year = today.getFullYear();

    this.today = year + '-' + fmonth + '-' + day;


  }

  getroutedata() {
    Storage.get({ key: 'route' }).then((val) => {
      this.route = JSON.parse(val.value);
  
    });
  }

  async loadingbar() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: '載入中',
      duration: 5000,
      spinner: 'circular',
    });
    await loading.present();
  }

  getinfor() {
    this.routetri = [];

    if (!VehisericeService.routearr) {
      return;
    }
 
    this.routearr = VehisericeService.routearr;

    for (const [key, value] of Object.entries(this.routearr)) {
      this.data = value;
      this.data.routes.forEach((info) => {
        Storage.get({ key: info.name + 'countbelow' }).then((below) => {
          if (below.value == null) {
            var cbelow = '0';
          } else {
            var cbelow = below.value;
          }
          Storage.get({ key: info.name + 'countabove' }).then((above) => {
            if (above.value == null) {
              var cabove = '0';
            } else {
              var cabove = above.value;
            }
            var input = {
              id: info.name,
              rname: info.long_name.split(/[a-zA-Z0-9]{1,}/gm)[0],
              peopleover: [],
              peoplebelow: [],
              countabove: cabove,
              countbelow: cbelow,
              readId:key
            };
            this.routetri.push(input);
            this.routetri.sort((a, b) => {
              return a.id - b.id;
            });
          });
        });
      });
    }

    setTimeout(() => (this.route = this.routetri), 1000);
  }
}
