import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CognitoService} from "../api/cognito.service";
import {AlertController } from "@ionic/angular";

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.page.html',
  styleUrls: ['./confirm.page.scss'],
})
export class ConfirmPage implements OnInit {
  phone: string;
  code: any;

  constructor(public alertController: AlertController,
    public router: Router,
    public CognitoService: CognitoService) { }

  ngOnInit() {
  }
  func(){
    this.phone = localStorage.getItem('phone')
     // localStorage.getItem('phone');
     console.log(this.phone,this.code);
     this.verifyUser(String(this.code), this.phone);
   }
 resendsms(){
  this.phone = localStorage.getItem('phone')
   console.log("resend")
 this.CognitoService.resendsms(this.phone)
 }
   verifyUser(verificationCode ,phone) {
     this.CognitoService.confirmUser(verificationCode,phone).then(
       res => {
         console.log(res);
         this.auth_verify();
         this.router.navigateByUrl('/tabs/tab2');
       },
       err => {
         alert(err.message);
       }
     );
   }
 
   async auth_verify() {
     const alert = await this.alertController.create({
       cssClass: 'my-custom-class',
       header: '認証',
       buttons: ['OK']
     });
 
     await alert.present();
   }
}
