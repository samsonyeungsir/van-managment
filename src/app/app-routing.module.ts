import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'setting',
    loadChildren: () => import('./setting/setting.module').then( m => m.SettingPageModule)
  },{
    path: '',
    redirectTo: 'splash',
    pathMatch: 'full'
  },
  {
    path: 'forgetpassword',
    loadChildren: () => import('./forgetpassword/forgetpassword.module').then( m => m.ForgetpasswordPageModule)
  }, 
  {
    path: 'routedetail',
    loadChildren: () => import('./routedetail/routedetail.module').then( m => m.RoutedetailPageModule)
  },
 {
    path: 'tab1',
    loadChildren: () => import('./tab1/tab1.module').then( m => m.Tab1PageModule)
  },
  {
    path: 'report',
    loadChildren: () => import('./report/report.module').then( m => m.ReportPageModule)
  },
  {
    path: 'enterpassword',
    loadChildren: () => import('./enterpassword/enterpassword.module').then( m => m.EnterpasswordPageModule)
  },
  {
    path: 'appoint',
    loadChildren: () => import('./appoint/appoint.module').then( m => m.AppointPageModule)
  },
  {
    path: 'gnewuser',
    loadChildren: () => import('./gnewuser/gnewuser.module').then( m => m.GnewuserPageModule)
  },
  {
    path: 'confirm',
    loadChildren: () => import('./confirm/confirm.module').then( m => m.ConfirmPageModule)
  },
  {
    path: 'splash',
    loadChildren: () => import('./splash/splash.module').then( m => m.SplashPageModule)
  },
  {
    path: 'vanvideo',
    loadChildren: () => import('./vanvideo/vanvideo.module').then( m => m.VanvideoPageModule)
  },
  {
    path: 'reportdetail',
    loadChildren: () => import('./reportdetail/reportdetail.module').then( m => m.ReportdetailPageModule)
  }






];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
